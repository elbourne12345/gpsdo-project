

// include the library code:
#include <string.h>
#include <ctype.h>
#include <avr/interrupt.h>  
#include <avr/io.h>
#include <Wire.h>
 



// Set up MCU pins
#define ppsPin                   2        // from GPS 1PPS 
#define ALARM_LED                LED_BUILTIN  // Only one user LED 
#define INT_LED          LED_BUILTIN 
#define LOCK_LED                 4
#define OPEN_LOOP                8
#define COUNT_PIN                5
#define DAC_HIGH                 6
#define DAC_LOW                  7

#ifdef lcd
#define RS                       7 // LCD RS
#define E                        8 // LCD Enable
#define DB4                      9 // LCD DB4
#define DB5                     10 // LCD DB5
#define DB6                     11 // LCD DB6
#define DB7                     12 // LCD DB7
#endif
  


#define CAL_FREQ  2500000UL      // In Hz, maximum is 4.5MHz - THIS IS THE SET FREQUENCY - 40mHZ / 16 FROM DIVIDER


#define MAX_TIME  (int)(10000000LL  / CAL_FREQ )    // Time to count 10 million billion counts, with 2.5MHz this is  4 seconds

#define SMALL_STEP    10          // 10e-10

#define TEST  false
//#define DEBUG                     // Verbose messages       
#undef DEBUG                      // No debug messages

// Variables 
byte res,Epos;
byte CptInit=1;
char StartCommand2[7] = "$GPRMC",buffer[300] = "";
int IndiceCount=0,StartCount=0,counter=0,indices[13];
int validGPSflag = 1;           // Set to 1 to avoid waiting for a valid GPS string in case no connection between GPS and Arduino
int Chlength;
int byteGPS=-1,second=0,minute=0,hour=0;
int64_t mult=0;               // Count of overflows of 16 bit counter
int alarm = 0;
unsigned int pulse_count=0;         // counts the seconds in a measurement cycle 
int duration = 4;                 // Initial measurement duration
int target_duration = 4;          // Value of duration for next measurement cycle
int count_available = 0;            // Flag set to 1 after pulse counting finished
int speed_available = 0;
int stable_count = 0;             // Count of consecutive measurements without correction needed.


long dac_out = 2047; 
int cat = 0;// Nominal for JNGPSDO (1.65v = not pulling VCXO)
long  dac_manual = 0;
#if 0
long dac_base = 30257;
float dac_mult = 0.025;
#else
#define DAC_RANGE 4096        // This range is used for MCP4725 DAC
long dac_base = DAC_RANGE/2 - 1;    // This base is used for MCP4725
float dac_mult = 1;
float dac_tmp = 0.0 ;
float dac_tmp2 = 0.0 ;
float count_corr = 0.01;      // Counts tend to be 1% high
#endif

#define DAC_CF 500000.0
#define MAX_CALFACT (DAC_RANGE/2)
#define MIN_CALFACT (-DAC_RANGE/2)
#include <dht.h>

dht DHT;

#define DHT11_PIN 7


int32_t calfact_x10     = 0;  // Current correction factor in 1/100 Hz
int32_t prev_calfact_x10  = 0;
int64_t target_count    = 0;  // Target count of pulses
int64_t measured_count    = 0;  // Actual count of pulses


int p_delta_sum = 0;

float high_gain =3.0e-7 / (float)256;             // Minimum correction step of high DAC
float gain = (float)3.0/(float)200000000.0 / (float)4096;   // Minimum correction step of low DAC
float corr = 0.0;
float min_corr = 0.0;
 

int force_duration = 0;
unsigned int low_count = 0;
int64_t high_count = 0;
unsigned int prev_low_count = 0;
int64_t prev_high_count = 0;
int64_t prev_count = 0;

int64_t delta_count = 0;
int64_t old_delta_count = 0;
int update = false;     // NR

 


// Timer 1 overflow interrupt vector.
ISR(TIMER1_OVF_vect) 
{
  mult++;             //Increment overflow multiplier
  high_count++;
  //TIFR1 = (1<<TOV1);    //Clear overflow flag by shifting left NR - not necessary, auto cleared
}

 



/* Clock - interrupt routine for counting the CLK0 2.5 MHz signal
Called every second by GPS 1PPS on Arduino Nano D2
 */
void PPSinterrupt()
{
  
#if defined(DEBUG) || defined(DEBUG_PPS)
  Serial.println(F("DBG - 1PPS Interrupt"));
#endif  


  // Maintain count of VCXO pulses
  // 
  low_count = TCNT1;
  //TCNT1 = 1;              // For Test - FYI will cause an overflow INT if zeroed

 


#ifdef DEBUG_COUNT
  Serial.print(F("hi_count=")); String str_hicnt = ToString(high_count);  Serial.print(str_hicnt);
  Serial.print(F("  prevhi_count=")); String str_phicnt = ToString(prev_high_count);  Serial.print(str_phicnt);
  Serial.print(F("  low_count=")); String str_lowcnt = ToString(low_count);  Serial.print(str_lowcnt);
  Serial.print(F("  prevlow_count=")); String str_plowcnt = ToString(prev_low_count);  Serial.println(str_plowcnt);
#endif  


  // NR - This is a problem as the timer will roll over and produce negative numbers 
  //delta_count = (int64_t)low_count  - (int64_t)prev_low_count + (high_count - prev_high_count) * 0x10000LL;
  delta_count = abs((int64_t)low_count  - (int64_t)prev_low_count) + (high_count - prev_high_count) * 0x10000LL;

  // Correction for continued count during 1PPS processing
  if ((abs(delta_count - CAL_FREQ)) > 500) delta_count = old_delta_count;   
  old_delta_count = delta_count;  

 prev_high_count = high_count;
  prev_low_count = low_count;
  
#if defined(DEBUG) || defined(DEBUG_PPS)
  String str = ToString(delta_count);  Serial.println(str);
#endif

// Pulse count
  if (count_available == 0) {
    if (pulse_count == 0)
      measured_count = 0;     // Add pulses
    pulse_count++;            // Increment the seconds counter
#ifdef DEBUG_COUNT
  Serial.print(F("pulse_count=")); String str_pcnt = ToString(pulse_count);  Serial.print(str_pcnt);
  Serial.print(F("  timer_count=")); String str_tcnt = ToString(low_count);  Serial.print(str_tcnt);
  Serial.print(F("  delta_count=")); String str = ToString(delta_count);  Serial.println(str);
#endif  
    measured_count += delta_count;          // Add pulses
    if (pulse_count >= target_duration)  // NR      // Stop the counter : the target_duration gate time is elapsed
    //if (pulse_count >= 4)       // Stop the counter : the target_duration gate time is elapsed
    {     
      duration = target_duration;
      target_count=((int64_t)CAL_FREQ)*duration;    // Calculate the target count     
      count_available = 1;
    update = true;                                // NR Ready to update frequency
#if defined(DEBUG) || defined(DEBUG_PPS) || defined(DEBUG_COUNT)
  //Serial.print(F("pulse_count=")); String str_pcnt = ToString(pulse_count);  Serial.println(str_pcnt);
  Serial.print(F("target_count=")); String str_tcnt = ToString(target_count);  Serial.print(str_tcnt);
  Serial.print(F(" measured=")); String str_mcnt = ToString(measured_count);  Serial.println(str_mcnt);
#endif
  pulse_count = 0;
    }
  } 
  
}



String ToString(int64_t x)    // Very clumsy conversion of 64 bit numbers
{
  boolean flag = false; // For preventing string return like this 0000123, with a lot of zeros in front.
  String str = "";      // Start with an empty string.
  uint64_t y = 10000000000000000000;
  int res;
  if (x<0) {
    x = -x;
    str = "-";
  }
  if (x == 0)       // if x = 0 and this is not testet, then function return a empty string.
  {
    str = "0";
    return str;     // or return "0";
  }    
  while (y > 0)
  {                
    res = (int)(x / y);
    if (res > 0)      // Wait for res > 0, then start adding to string.
      flag = true;
    if (flag == true)
      str = str + String(res);
    x = x - (y * (int64_t)res);   // Subtract res times * y from x
    y = y / 10;                     // Reducer y with 10    
  }
  return str;
}  




// Setuup the system
void setup()
{

  Serial.begin(57600);  // Define the GPS port speed

#ifdef DEBUG
  Serial.println(F("DBG - Doing Setup"));
#endif  


  
  // Set up timer1 registers
  TCCR1B  = 0;
  TCCR1A  = 0;
  TCNT1   = 0;
  TIFR1   = (1<<TOV1);    // Clear OVF 
  TIMSK1  = (1<<TOIE1);   // Interrupt Enable



  // GPS 1pps input
  pinMode(ppsPin, INPUT);
  analogReference(INTERNAL);
  //displ_setCursor(0,1);
  //displ_print(F(" F2DC V.5.2"));  // display version number on the LCD
  delay(1000);


#ifdef DEBUG
  Serial.println(F("DBG - Attaching to interrupt"));
#endif  

  // Set Arduino D2 for external interrupt input on the rising edge of GPS 1PPS
  attachInterrupt(digitalPinToInterrupt(ppsPin), PPSinterrupt, RISING);  
  // For DEBUG 
  //attachInterrupt(digitalPinToInterrupt(ppsPin), dbisr, RISING);  
  Serial.print("Attached to: ");  Serial.print(digitalPinToInterrupt(ppsPin)); Serial.println("");
  
 
    TCCR1B    = 0;    //Turn off Counter
    TCNT1     = 0;    //Reset counter to zero
    low_count   = 0;
    high_count  = 0;
    //TCCR1B    = 7;    //Clock on rising edge of pin 5
    TCCR1B = (1<<CS12) | (1<<CS11) | (1<<CS10);  // Clock on rising edge of Pin 5


  // Turn OFF CLK2 
  //  SI5351.output_enable(SI5351_CLK2,0); 

  // Set up parameters

  target_count=CAL_FREQ*duration;


  Serial.print(F("Target Frequency (CAL_FREQ) = ")); Serial.println((CAL_FREQ));
  Serial.println(F("Waiting for GPS 1PPS"));
}



//******************************************************************
// Loop 
void loop()
{
  
  String str = "";
  int64_t target_freq,actual_freq;
  

  if (validGPSflag == 0) 
    int gil; // GPS flag and gil mean absoulutely nothing, just placeholders for now whilr testing code.
  else if(cat = 0) // Frequency calculation data count_available                                   
  {
    
#ifdef DEBUG_CORR   
    Serial.print(F("Using count correction only.  Measured: "));
    str = ToString(measured_count);  Serial.print(str);
  Serial.print(F("  Target: "));
    str = ToString(target_count);  Serial.println(str);
#endif
    
  // JNGPSDO - Corrections done here...  
    // digitalWrite(ALARM_LED, LOW);


    if (force_duration)
      target_duration = force_duration;

  }



  if (update) {
 

 
    Serial.print(F(" calfact="));
    Serial.print(calfact_x10);
    Serial.print(F(" acount="));
    str = ToString(measured_count);  Serial.print(str);

    Serial.print(F(" mincorr="));
    int chk = DHT.read11(DHT11_PIN);
    Serial.print("Temperature = ");
    Serial.println(DHT.temperature);
    Serial.print("Humidity = ");
    Serial.println(DHT.humidity);
    int e = 0;
    float f = min_corr;



    prev_calfact_x10 = calfact_x10;
 

 
     

  }
  if(update) {
#ifdef DEBUG_COUNT    
    Serial.println(F("DEBUG - Resetting...")); 
#endif
    pulse_count = 0;          // Restart count measurement
    count_available= 0;       // Restart

    corr = 0;
  update = false;       // NR - ok
  }


}
      
